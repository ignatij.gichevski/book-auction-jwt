package com.finki.soa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finki.soa.model.Person;
import com.finki.soa.repository.PersonRepository;

@Service
public class PersonService {

	@Autowired
	private PersonRepository personRepository;

	public Person findByUsername(String username) {
		return personRepository.findByUsername(username);
	}

	public Iterable<Person> findAll() {
		return personRepository.findAll();
	}

	public Person findOne(Long id) {
		return personRepository.findOne(id);
	}


}
