package com.finki.soa.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.finki.soa.model.Book;
import com.finki.soa.model.Offer;
import com.finki.soa.model.Person;
import com.finki.soa.service.BookService;
import com.finki.soa.service.OfferService;
import com.finki.soa.service.PersonService;

@RestController
public class OfferController {

	@Autowired
	private OfferService offerService;

	@Autowired
	private BookService bookService;

	@Autowired
	private PersonService personService;

	@GetMapping("/offer")
	public Iterable<Offer> getOffers() {
		return offerService.findAll();
	}

	@GetMapping("/offer/book/{bookName}")
	public List<Offer> getOffersByBookName(@PathVariable String bookName) {
		return offerService.findAllByBook(bookService.findBookByName(bookName));
	}

	@PostMapping("/offer")
	public Iterable<Offer> makeOffer(OfferModel makeOffer, HttpServletRequest httpServletRequest) {
		Person person = personService.findByUsername(makeOffer.getUsername());
		Book book = bookService.findBookByName(makeOffer.getBookName());
		return offerService.saveOffer(book, person, Integer.parseInt(makeOffer.getAmount()));
	}

	private static class OfferModel {
		private String bookName;
			private String username;
		private String amount;

		public String getBookName() {
			return bookName;
		}

		public void setBookName(String bookName) {
			this.bookName = bookName;
		}

		public String getAmount() {
			return amount;
		}

		public void setAmount(String amount) {
			this.amount = amount;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getUsername() {
			return username;
		}

	}

}
