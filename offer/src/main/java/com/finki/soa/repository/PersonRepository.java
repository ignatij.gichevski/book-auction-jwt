package com.finki.soa.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.finki.soa.model.Person;

public interface PersonRepository extends CrudRepository<Person, Long>{
	
	Person findByUsername(String username);

}
