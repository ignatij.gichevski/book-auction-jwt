package com.finki.soa.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.finki.soa.model.Person;
import com.finki.soa.service.PersonService;

@Controller
public class LoginController {

	@Autowired
	private PersonService personService;

	@GetMapping("/")
	public String welcomePage(HttpServletRequest httpServletRequest) {
		return "index";
	}

	@GetMapping("/login")
	public String loginPage(HttpServletRequest httpServletRequest) {
		return "login_page";
	}

	@GetMapping("/register")
	public String registerPage(HttpServletRequest httpServletRequest) {
		return "register_page";
	}

	@PostMapping(value = "/register")
	public String doRegister(RegisterForm registerForm, HttpServletRequest httpServletRequest) {
		String username = registerForm.getUsername();
		if (personService.findByUsername(username) == null) {
			Person p = new Person();
			p.setFirstName(registerForm.getFirstName());
			p.setSecondName(registerForm.getSecondName());
			p.setUsername(username);
			p.setPassword(registerForm.getPassword());
			personService.save(p);
			return "redirect:/login";
		} else
			return "redrect:/register";
	}

	@PostMapping("/logout")
	public String logout(HttpServletRequest httpServletRequest) {
		return "redirect:/";
	}

	private static class LoginForm {
		private String username;
		private String password;

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

	}

	private static class RegisterForm {
		private String username;
		private String password;
		private String firstName;
		private String secondName;

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getSecondName() {
			return secondName;
		}

		public void setSecondName(String secondName) {
			this.secondName = secondName;
		}

	}

}
